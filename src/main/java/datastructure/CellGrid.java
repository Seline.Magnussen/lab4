package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int cols;
    CellState initialState;
    CellState[][] grid;


    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        this.initialState = initialState;
        grid = new CellState[numRows()][numColumns()];
        for (int row = 0; row < numRows(); row++) {
            for (int col = 0; col < numColumns(); col++) {
                grid[row][col] = initialState;

            }
        }

    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if ((row < 0 || row >= numRows()) || (column < 0 || column >= numColumns())) {
            throw new IndexOutOfBoundsException();
        }
        grid[row][column] = element;

    }

    @Override
    public CellState get(int row, int column) {
        if ((row < 0 || row >= numRows()) || (column < 0 || column >= numColumns())) {
            throw new IndexOutOfBoundsException();
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid copyGrid = new CellGrid(rows, cols, initialState);
        for (int row = 0; row < numRows(); row++) {
            for (int col = 0; col < numColumns(); col++) {
                copyGrid.set(row, col, grid[row][col]);
            }
        }
            return copyGrid;

    }
}
