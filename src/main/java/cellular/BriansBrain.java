package cellular;


public class BriansBrain extends GameOfLife {
    /**
     * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
     * provided size
     *
     * @param rows
     * @param columns
     */
    public BriansBrain(int rows, int columns) {
        super(rows, columns);
    }
    public CellState getNextCell(int row, int col) {
        CellState cState = getCellState(row, col);
        int livingNeighbours = countNeighbors(row, col, CellState.ALIVE);
        if (cState.equals(CellState.ALIVE)) {
            cState = CellState.DYING;
        }
        else if (cState.equals(CellState.DYING)) {
            cState = CellState.DEAD;
        }
        else if (cState.equals(CellState.DEAD) && livingNeighbours == 2) {
            cState = CellState.ALIVE;
        }
        else if (cState.equals(CellState.DEAD)) {
            cState = CellState.DEAD;
        }
        return cState;
    }
}

