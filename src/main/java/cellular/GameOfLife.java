package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	int row;
	int col;

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
		this.row = rows;
		this.col = columns;
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return this.row;
	}

	@Override
	public int numberOfColumns() {
		return this.col;
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < nextGeneration.numRows(); row++) {
			for (int col = 0; col < nextGeneration.numColumns(); col++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		currentGeneration = nextGeneration;
		}
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState cellStatus = getCellState(row, col);
		int livingNeighbours = countNeighbors(row, col, CellState.ALIVE);
		if (cellStatus.equals(CellState.ALIVE) && livingNeighbours == 2) {
			cellStatus = CellState.ALIVE;
		}
		else if (cellStatus.equals(CellState.ALIVE) && livingNeighbours == 3) {
			cellStatus = CellState.ALIVE;
		}
		else if (cellStatus.equals(CellState.DEAD) && livingNeighbours == 3) {
			cellStatus = CellState.ALIVE;
		}

		else
			cellStatus = CellState.DEAD;

			return cellStatus;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	public int countNeighbors(int row, int col, CellState state) {
		int neighbourCount = 0;
		for (int i = row - 1; i <= row + 1; i++) {
			for (int k = col - 1; k <= col + 1; k++) {
				if (i >= currentGeneration.numRows() || k >= currentGeneration.numColumns()) {
					continue;
				}
				else if ((i < 0 || k < 0)) {
					continue;

				}
				else if (i == row && k == col) {
					continue;

				} else if (currentGeneration.get(i, k).equals(state)) {
					neighbourCount++;
				}
			}

			}
			return neighbourCount;
		}

		@Override
		public IGrid getGrid () {
			return currentGeneration;
		}

	}